


/// <reference types="cypress" />

/*
import settings from "./settings"
import loc from "./locators"
*/

const dayjs = require('dayjs')
// https://day.js.org/docs/en/plugin/utc
const utc = require('dayjs/plugin/utc')
// https://day.js.org/docs/en/plugin/custom-parse-format
const customParseFormat = require('dayjs/plugin/customParseFormat')

dayjs.extend(utc)
dayjs.extend(customParseFormat)


let token

function login() {

}

describe('BarrigaRest', () => {


  //https://barrigarest.wcaquino.me/signin
  //POST, payload
  //{"email":"daniel_arend@hotmail.com","senha":"Teste123","redirecionar":false}

  before(() => {
    cy.getToken('daniel_arend@hotmail.com', 'Teste123').then(tkn => {
      token = tkn
    })

  })

  beforeEach(() => {
    cy.resetRest()
  })

  it('Login', () => {
    login()



  })

  it('nova conta ', () => {

    cy.request({
      method: "POST",
      url: '/contas',
      headers: {
        Authorization: `JWT ${token}`
      },
      body: {
        nome: 'Conta via rest #9'
      }
      //magica acontece aqui com o alias...
      //declarando assim, podemos desaninhar a chamada do response
      //pelo alias
    }).as('response')
    cy.get('@response').then(res => {
      expect(res.status).equal(201);
      expect(res.body).to.have.property('id');
    })

  })

  it('editar contas', () => {

    /*
    

*/
    cy.getContaByName('Conta para movimentacoes').then(id => {


      cy.request({
        url: `/contas/${id}`,
        method: 'PUT',
        headers: {
          Authorization: `JWT ${token}`
        },
        body: {
          nome: 'Conta alterada via rest XX'
        }

      }).as('response')
      cy.get('@response').its('status').should('equal', 200)

    })
  })



  it('conta duplicada ', () => {

    cy.request({
      method: "POST",
      url: '/contas',
      headers: {
        Authorization: `JWT ${token}`
      },
      body: {
        nome: 'Conta mesmo nome'
      },
      failOnStatusCode: false
      //magica acontece aqui com o alias...
      //declarando assim, podemos desaninhar a chamada do response
      //pelo alias
    }).as('response')
    cy.get('@response').then(res => {
      console.log(res);
      expect(res.status).equal(400);
      expect(res.body).to.have.property('error');
    })

  })


  it('inserir movimentacao ', () => {


    cy.getContaByName('Conta para movimentacoes').then(id => {
      cy.request({
        method: "POST",
        url: '/transacoes',
        headers: {
          Authorization: `JWT ${token}`
        },
        body: {
          tipo: "REC",
          data_transacao: dayjs().format('DD/MM/YYYY'),
          data_pagamento: dayjs().format('DD/MM/YYYY'),
          descricao: "conta teste",
          valor: "125",
          envolvido: "chaves",
          conta_id: id,
          status: true
        },
        //failOnStatusCode: false
        //magica acontece aqui com o alias...
        //declarando assim, podemos desaninhar a chamada do response
        //pelo alias
      }).as('response')
      cy.get('@response').then(res => {
        console.log(res)
        expect(res.status).equal(201)
        expect(res.statusText).equal('Created')
      })


    })


  })



})