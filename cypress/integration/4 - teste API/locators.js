const locators = {

  login: {
    email: '.jumbotron [data-test="email"]',
    password: '.jumbotron [data-test="passwd"]'
  }

}

export default locators;