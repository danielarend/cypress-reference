/// <reference types="cypress" />


describe('mexendo com elementos basicos', () => {

  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })

  beforeEach(() => {
    cy.reload();
  })
  it('text', () => {
    cy.get('body span.facilAchar').should('contain', 'Cuidado');
    // cy.get('body span.facilAchar').should('have.text', 'Cuidado');
  });

  it('links', () => {
    cy.get('center a[href="#"]').click();
    cy.get('#resultado').should('have.text', 'Voltou!');
    cy.reload();
    cy.get('#resultado').should('not.have.text', 'Voltou!');
    cy.get('center a[href="#"]').click();
    cy.get('#resultado').should('have.text', 'Voltou!');

  });


  it('text fields', () => {

    cy.get('#formNome').type('Cypress Test');
    cy.get('#formNome').should('have.value', 'Cypress Test');
    cy.get('#elementosForm\\:sugestoes')
      .type('textarea')
      .should('have.value', 'textarea')


    cy.get('#tabelaUsuarios tbody tr:first-child input[type="text"').type('lala lele{backspace}{backspace}')

    cy.get('#elementosForm\\:sugestoes')
      .clear()
      .type('erro {selectall}acerto', { delay: 100 })
      .should('have.value', 'acerto')


  });

  it('radios', () => {

    cy.get('#formSexoMasc')
      .click()
      .should('be.checked')
      .should('have.value', 'M')
    cy.get("[name='formSexo']").should('have.length', 2)

  });

  it('checkboxes', () => {

    cy.get('#formComidaPizza')
      .click()
      .should('be.checked')


    cy.get("[name='formComidaFavorita']")
      .click({ multiple: true })
      .should('be.checked')

  });

  it.only('dropdowns', () => {

    cy.get('#formEscolaridade')
      .select('2o grau completo')
      .should('have.value', '2graucomp')




    cy.get('#formEscolaridade option').should('have.length', 8)

    cy.get('#formEscolaridade option').then($arr => {

      const values = [];
      $arr.each(function () {
        values.push(this.innerHTML);
      })
      expect(values).to.include.members(["Superior", "Mestrado"])
    }



    )

    //multiplo
    cy.get('#formEsportes')
      .select(['natacao', 'futebol'])
    //.should('have.value', '2graucomp')
    //cy.get('#formEsportes').should('have.value', 'nada')

    cy.get('#formEsportes').then($el => {
      expect($el.val()).to.be.deep.equal(['natacao', 'futebol'])
      expect($el.val()).to.have.length(2);

    })
    //eql = deeply equal, se trocar por equal, da pau...
    cy.get('#formEsportes').invoke('val').should('eql', ['natacao', 'futebol'])


  });




});
/*

  it('exemplo', () => {


  });


describe('externo...', () => {

  before(() => {
    cy.visit('https://wcaquino.me/cypress/frame.html')
  })
  it('externo', () => {




  });

});
*/
