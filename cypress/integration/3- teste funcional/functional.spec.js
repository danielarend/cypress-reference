


/// <reference types="cypress" />

import settings from "./settings"
import loc from "./locators"

function login() {

  cy.visit('https://barrigareact.wcaquino.me')
  cy.get(loc.login.email).type(settings.user.email)
  cy.get(loc.login.password).type(settings.user.password)
  cy.get('.jumbotron button').click()
  cy.get('.toast-message').should('contain.text', 'Bem vindo')
  cy.get('.toast-info button').click()


}
function newAccount() {
  const account_id = parseInt(Math.random() * 1000000);
  cy.get('.navbar .dropdown-toggle').click()
  cy.get('.navbar a[href="/contas"]').click()
  cy.get('input[data-test="nome"]').type(`Minha conta #${account_id}`)
  cy.get('.container .form-group button').click()
  cy.get('.toast-message').should('contain.text', 'Conta inserida com sucesso!')
}

function logout() {
  cy.visit('https://barrigareact.wcaquino.me/logout', { failOnStatusCode: false })
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

describe('BarrigaReact - Nova conta', () => {

  before(() => {
    logout()
    login()
  })

  beforeEach(() => {
    // cy.reload();
  })

  it('nova conta', () => {
    newAccount()
  })

  it('editar conta', () => {

    newAccount()
    cy.get('table tbody > tr:nth-child(1) .fa-edit').click()
    const edit_id = parseInt(Math.random() * 1000000);
    cy.get('input[data-test="nome"]').type(`- edit ${edit_id}`)
    cy.get('.container .form-group button').click()
    cy.get('.toast-message').should('contain.text', 'Conta inserida com sucesso!')
    cy.get('.toast-success button').click()

  })


  it('conta repetida', () => {

    const account_id = parseInt(Math.random() * 1000000);
    cy.get('.navbar .dropdown-toggle').click()
    cy.get('.navbar a[href="/contas"]').click()
    cy.get('input[data-test="nome"]').type(`Minha conta #${account_id}`)
    cy.get('.container .form-group button').click()
    cy.get('.toast-message').should('contain.text', 'Conta inserida com sucesso!')
    cy.get('.toast-success button').click()
    cy.get('input[data-test="nome"]').clear()
    cy.get('input[data-test="nome"]').type(`Minha conta #${account_id}`)
    cy.get('.container .form-group button').click()
    cy.get('.toast-message').should('contain.text', 'Request failed')
    cy.get('.toast-error button').click()

  })

  it('nova movimentacao', () => {

    const movimento_id = parseInt(Math.random() * 1000000);
    cy.get('.navbar .dropdown-toggle').click()
    cy.get('.navbar a[href="/movimentacao"]').click()
    cy.get('#descricao').type(`Minha movimentacao #${movimento_id}`)

    cy.get('[data-test="valor"]').type(123)
    cy.get('#envolvido').type(`Envolvido #${movimento_id}`)
    cy.get('[data-test="status"]').click()

    cy.get(`[data-test="conta"]> option`)

    cy.get('select.form-control[data-test="conta"] > option').then($listing => {
      const randomNumber = getRandomInt(0, $listing.length);
      cy.get('select.form-control[data-test="conta"] > option').eq(randomNumber).then(($select) => {
        const text = $select.text()
        cy.get(`select.form-control[data-test="conta"]`).select(text)
        cy.get('.container .btn-primary').click()
        cy.get('.toast-message').should('contain.text', 'Movimentação inserida com sucesso!')

      });

    });

  })

  it('saldo', () => {

    cy.get('.navbar a[href="/"]').click()
    cy.get('table thead tr th').should('contain', 'Saldo');
    //isso chama o retry por um tempo até que a tabela tenha o tamanho certo
    cy.get('table tbody tr').should('have.length.greaterThan', 1).then(($row) => {

      let soma = 0;
      let total = 0;

      let objSoma = $row.find('td:last-of-type');
      objSoma.each(function (index) {
        console.log(this.innerText)
        if (index == objSoma.length - 1) {
          total = parseFloat(this.innerText.replace('R$', '').replace('.', ''));
        } else {
          soma += parseFloat(this.innerText.replace('R$', '').replace('.', ''));
        }
      });

      expect(soma).equal(total)

    })
  })


  it('remover movimentacao', () => {

    cy.get('.navbar a[href="/extrato"]').click()
    cy.get('.list-group li').should('have.length.greaterThan', 0).then(($item) => {
      //tenta removera primeira da lista
      cy.wrap($item[0].querySelector('.fa-trash-alt')).click();
      cy.get('.toast-message').should('contain.text', 'Movimentação removida com sucesso!')
    })
  })

})