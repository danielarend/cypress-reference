/// <reference types="cypress" />


describe('teste basico', () => {

  it('should visit a page and asser title', () => {
    /*
    https://wcaquino.me/cypress/componentes.html
    */

    cy.visit('https://wcaquino.me/cypress/componentes.html')
    console.log(cy.title());

    cy.title().should('equal', 'Campo de Treinamento')
      .should('contain', 'Campo')
      .and('contain', 'Campo')
      .then((x) => {
        console.log(x)
      })
    cy.title().then(title => {
      console.log(title);
    })
  });

});

describe('botao', () => {

  it('queryselector em elementos', () => {
    /*
    https://wcaquino.me/cypress/componentes.html
    */

    cy.visit('https://wcaquino.me/cypress/componentes.html')

    cy.get('#buttonSimple').click()
    cy.get('#buttonSimple').should('have.value', 'Obrigado!');


  });

});


