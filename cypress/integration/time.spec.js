/// <reference types="cypress" />



describe('time...', () => {

  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })

  beforeEach(() => {
    cy.reload();
  })

  it('time - voltando ao passado', () => {

    cy.get('#buttonNow').click()
    //cy.get('#resultado > span').should('contain', '30/11/2021')
    //volta pra 1969
    //  cy.clock()

    const dt = new Date(2021, 1, 2, 11, 5, 12, 0)
    cy.clock(dt.getTime())

    cy.get('#buttonNow').click()
    cy.get('#resultado > span').should('contain', '02/02/2021')
  })

  it('time - vai pro futuro', () => {


    cy.get('#buttonTimePassed').click();
    cy.get('#resultado span').then($el => {
      const value = parseInt($el.text());
      expect(value).to.be.greaterThan(1638287500024)

    })
    cy.clock()
    cy.get('#buttonTimePassed').click()

    cy.get('#resultado span').then($el => {
      const value = parseInt($el.text());
      expect(value).equal(0)

    })

    cy.tick(5000)
    cy.get('#buttonTimePassed').click()

    cy.get('#resultado span').then($el => {
      const value = parseInt($el.text());
      expect(value).equal(5000)

    })



  })

})