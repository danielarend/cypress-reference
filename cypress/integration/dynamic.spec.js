/// <reference types="cypress" />



describe('dynamic...', () => {

  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })

  beforeEach(() => {
    cy.reload();
  })

  const foodList = ['Carne', 'Frango', 'Pizza', 'Vegetariana']










  for (let food of foodList) {

    it('validando campos:' + food, () => {

      cy.get('#formNome').type('usuario')
      cy.get('#formSobrenome').type('sobrenome')
      cy.get('#formSexoMasc').click()
      cy.get(`#formComida${food}`).check()
    })
  }

  it.only('deve selecionar todos usand each', () => {

    cy.get('#formNome').type('usuario')
    cy.get('#formSobrenome').type('sobrenome')
    cy.get('#formSexoMasc').click()
    cy.get(`#formComidaFavorita input`).each($el => {

      if ($el.val() != 'vegetariano')
        cy.wrap($el).click();
    })

    cy.clickAlert('#formCadastrar', 'Tem certeza que vc eh vegetariano?')

    cy.get('#formCadastrar').click()
    cy.get('#resultado').should('not.have.text', 'Status: Nao cadastrado');
  })


  /*
  cy.get('#formEscolaridade').select(usuario.escolaridade);

  cy.get('#formEsportes').select(usuario.esportes);

  cy.get('#formCadastrar').click()
  cy.get('#resultado').should('not.have.text', 'Status: Nao cadastrado');
  */





})