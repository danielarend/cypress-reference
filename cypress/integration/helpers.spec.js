describe('helpers...', () => {
  /*
  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })

  beforeEach(() => {
    cy.reload()
  })
  */

  it('wrap', () => {
    const obj = { nome: 'User', idade: 20 };

    expect(obj).to.have.property('nome');
    cy.wrap(obj).should('have.property', 'nome');



  });

  it('its', () => {


    //its pega as chaves dos objetos...
    const obj = { nome: 'User', idade: 20 };
    cy.wrap(obj).should('have.property', 'nome', 'User');
    cy.wrap(obj).its('nome').should('equal', 'User');
  });


  it('invoke', () => {

    //invoke executa funcoes js no cypress..
    const getValue = () => 1;
    const soma = (a, b) => a + b;
    cy.wrap({ fn: getValue }).invoke('fn').should('equal', 1);
    cy.wrap({ fn: soma }).invoke('fn', 2, 5).should('equal', 7);


    cy.visit('https://wcaquino.me/cypress/componentes.html')
    //val() vem do jquery
    cy.get('#formNome').invoke('val', 'texto teste')
    cy.window().invoke('alert', 'teste de alerta')
    cy.get('#resultado')
      .invoke('html', '<input type="button" value="testando">')
  });



})
/*
it('exemplo', () => {

});
*/