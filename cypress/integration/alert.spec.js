/// <reference types="cypress" />



describe('alerts', () => {

  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })

  beforeEach(() => {
    cy.reload();
  })
  it('Alert', () => {
    cy.get('#alert').click();
    cy.on('window:alert', msg => {
      console.log(msg);
      expect(msg).equal('Alert Simples');
    })
  })


  it('stub com alert', () => {

    const stub = cy.stub().as('alerta');

    cy.on('window:alert', stub);

    cy.get('#alert').click().then(() => {
      expect(stub.getCall(0)).to.be.calledWith('Alert Simples')
    })

  })

  it('confirm', () => {


    cy.on('window:confirm', msg => {
      console.log(msg);
      expect(msg).equal('Confirm Simples');
    })
    cy.on('window:alert', msg => {
      console.log(msg);
      expect(msg).equal('Confirmado');
    })

    cy.get('#confirm').click()

  })

  it('deny confirm', () => {

    //clicando no cancelar
    cy.on('window:confirm', msg => {
      console.log(msg);
      expect(msg).equal('Confirm Simples');

      //cancelando
      return false;
    })
    cy.on('window:alert', msg => {
      console.log(msg);
      expect(msg).equal('Negado');
    })

    cy.get('#confirm').click()

  })

  it('prompt', () => {
    cy.window().then(win => {
      cy.stub(win, 'prompt').returns('42')
    })

    cy.on('window:alert', msg => {
      console.log(msg);
      expect(msg).equal(':D');
    })

    cy.get('#prompt').click()

  })


  it('Alert usando coomand', () => {
    cy.clickAlert('#alert', 'Alert Simples')
  })

  it('desafio - alertas', () => {

    //1 - clocar e cadastrar. capturar a mensagem
    //2 - preencher o nodeName, tentar cadastrar, pega mensage.as
    //3 - sobrenome, mesmo
    //4 - sexoo mesma crossOriginIsolated, 
    //5 - pega a mensgagem de retoron do cadastro, quando da certo.
    // cadastrar somente o nome.


    const stub = cy.stub().as('alerta');
    cy.on('window:alert', stub);


    cy.get('#formCadastrar').click().then(() => {
      expect(stub.getCall(0)).to.be.calledWith('Nome eh obrigatorio')
    })

    cy.get('#formNome').type('meu nome');
    cy.get('#formCadastrar').click().then(() => {
      expect(stub.getCall(1)).to.be.calledWith('Sobrenome eh obrigatorio')
    })

    cy.get('#formSobrenome').type('meu sobrenome');
    cy.get('#formCadastrar').click().then(() => {
      expect(stub.getCall(2)).to.be.calledWith('Sexo eh obrigatorio')
    })
    cy.get('#formSexo').click();
    cy.get('#formCadastrar').click();
    cy.get('#resultado').should('not.have.text', 'Status: Nao cadastrado');




  })
})