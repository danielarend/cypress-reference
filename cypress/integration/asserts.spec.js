/// <reference types="cypress" />
it('equalty', () => {
  const a= 1;

  expect(a, "msg custom").equal(1);
  expect(a, 'deveria ser 1').equal(1);
  expect(a).to.equal(1);
  expect('a').not.to.be.equal('b');
  

})


it('verdadeiro', () => {
  const a = true;
  const b = null;
  expect(a).to.be.true;
  expect(a).not.equal(false);

  expect(b).equals(null);
  expect(b).not.equals(2);

})

it ('equalidade de objetos', () => {

  const obj = {
    a: 1,
    b: 2
  }

  expect(obj).equal(obj);
  expect(obj).deep.equal({a:1, b:2});
  //mesma coisa
  expect(obj).eql({a:1, b:2});
  expect(obj).not.eq({a:1, b:2});
  //auto explicativo
  expect(obj).include({a:1})

  expect(obj).to.have.property("b")
  expect(obj).to.have.property("b", 2)

  
  expect(obj).to.not.be.empty;


  


});

 
it ('arrays', () => {
  const arr = [1,2,3];
  expect(arr).to.have.members([1,2,3]);
  expect(arr).to.include.members([1,3]);
  expect(arr).to.not.be.empty;
  

});

it ('types', () => {

  const num = 1;
  const str = 'string';
  
  expect(num).to.be.a('number');
  expect(str).to.be.a('string');
  expect({}).to.be.a('object')
  expect({}).to.be.an('object')


});

it ('strings', () => {

  const str = 'string de teste';
  expect(str).not.equal('string te');
  expect(str).contains('string de');
  expect(str).to.match(/de/);
  //sem numeros
  expect(str).to.match(/\D+/);

});

it ('numbers ', ()=> {

  const number = 3;
  const float = 3.423;
  expect(number).equal(3);
  expect(number).above(2);
  expect(number).below(5);
  expect(float).equal(3.423);
  expect(float).closeTo(3.4, 0.1);
  
  


});