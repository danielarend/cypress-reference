/// <reference types="cypress" />


describe('sync...', () => {

  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })

  beforeEach(() => {
    cy.reload()
  })

  it('deve aguardar elemento estar disponivel', () => {
    cy.get('#novoCampo').should('not.exist');
    cy.get('#buttonDelay').click();
    cy.get('#novoCampo').should('exist');
    cy.get('#novoCampo').type('funciona');

  });



  it('deve fazer retries', () => {

    cy.get('#buttonDelay').click();
    cy.get('#novoCampo')
      .should('exist')
      .type('funciona')
  });

  it('uso do find', () => {
    cy.get('#buttonList')
      .click();
    cy.get('#lista li')
      .find('span')
      .should('contain', 'Item 1')
    /*
    find nao funciona na repeticao de itens encadeados na 
    resposta.
    
    cy.get('#lista li')
      .find('span')
      .should('contain', 'Item 2')
    */
    cy.get('#lista li span')
      .should('contain', 'Item 2')
  });

  it('uso do timeout', () => {
    cy.get('#buttonDelay').click();
    cy.get('#novoCampo', { timeout: 5000 })
      .should('exist')


    cy.get('#buttonListDOM').click();
    cy.wait(2000)
    cy.get('#lista li span')
      .should('have.length', 2);

  });

  it('click retry', () => {
    cy.get("#buttonCount").click().should('have.value', '1')
  });

  it.only('Should vs Then', () => {

    cy.get('#buttonListDOM').click();
    cy.get('#lista li span').debug();
    //ver console
    cy.get('#lista li span').should($element => {
      console.log($element);
      //cy.get('#buttonListDOM')
      //sendo ele um objeto jquery, 
      //nao aceita should, etc....

      expect($element).to.have.length(1);
    });

    cy.get('#buttonListDOM').then($element => {
      //console.log($element);

      //sendo ele um objeto jquery, 
      //nao aceita should, etc....

      expect($element).to.have.length(1);
      //return 2;
    }).and('have.id', 'buttonListDOM')


  });


});


/*

  it('exemplo', () => {
buttonList

  });


  */