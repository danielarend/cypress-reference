/// <reference types="cypress" />


describe('popup', () => {

  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })
  /*
   beforeEach(() => {
     cy.reload();
   })
   */
  it('deve verificar se o popop foi invocado', () => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')

    cy.window().then(win => {
      cy.stub(win, 'open').as('winOpen')
    })


    cy.get('#buttonPopUp').click();
    cy.get('@winOpen').should('be.called')

  })


  it('popup links - parte 2', () => {

    cy.contains('Popup2')
      .should('have.prop', 'href')
      .and('equal', 'https://wcaquino.me/cypress/frame.html')

  })
  it('should access popoup dinamiically', () => {

    cy.contains('Popup2').then($a => {
      const href = $a.prop('href')
      cy.visit(href)
      cy.get('#tfield').type('olar')
    })
  })

  it('force link at same page', () => {

    cy.contains('Popup2').then($a => {
      const href = $a.prop('href')
      cy.visit(href)
      cy.get('#tfield').type('olar')
    })
  })


})