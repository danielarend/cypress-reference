/// <reference types="cypress" />



describe('fixtures (dados mocados) 1', () => {

  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })

  beforeEach(() => {
    cy.reload();
  })
  it('xpath 1', () => {


    cy.fixture('userData.json').as('usuario').then((usuario) => {
      cy.get('#formNome').type(usuario.nome)
      cy.get('#formSobrenome').type(usuario.sobrenome)
      if (usuario.sexo == "M") {
        cy.get('#formSexoMasc').click()
      } else {
        cy.get('#formSexoFem').click()
      }
      const comida = `${usuario.comida[0].toUpperCase()}${usuario.comida.slice(1)}`

      cy.get(`#formComida${comida}`).check()
      cy.get('#formEscolaridade').select(usuario.escolaridade);

      cy.get('#formEsportes').select(usuario.esportes);

      cy.get('#formCadastrar').click()
      cy.get('#resultado').should('not.have.text', 'Status: Nao cadastrado');

    })




  })

})