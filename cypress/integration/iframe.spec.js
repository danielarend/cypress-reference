/// <reference types="cypress" />


describe('iframe', () => {
  /*
  before(() => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
  })

  beforeEach(() => {
    cy.reload();
  })
  */
  it('iframe - parte 1', () => {
    cy.visit('https://wcaquino.me/cypress/componentes.html')
    cy.get('#frame1').then(iframe => {
      const body = iframe.contents().find('body');
      cy.wrap(body.find('#tfield')).type('funciona?')
        .should('have.value', 'funciona?')
      // cy.wrap(body.find('#otherButton')).click();
    })
  })


  it.only('iframe - parte 2', () => {
    //workaround para iframe, diretamente nele
    cy.visit('https://wcaquino.me/cypress/frame.html')
    cy.get('#otherButton').click();
    cy.on('window:alert', msg => {
      expect(msg).equal('Click OK!')
    })

  })


})