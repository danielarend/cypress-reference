it ('sem testes', () => {})
/*
const getSomething = callback => {
  setTimeout(() => {
    console.log('respondendo');
    callback(12);
  }, 1000);
}

const system  = () => {
  console.log('init');
  getSomething(some => console.log(some));
  console.log(`haha ${something}`);
}
*/

const getSomething = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(13);
    }, 1000)
  })
}

const system  = async () => {
  console.log('init');
  const some = await getSomething();
  console.log(`haha ${some}`);


  console.log('end');

}



system();
